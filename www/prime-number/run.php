<?php
require __DIR__.'/../vendor/autoload.php';

$number = $argv[1];

if(!(int) $number){
    echo "Please, enter a integer number\n";
    die();
}
try{
    $prime = new PrimeNumber\Prime();
    $prime->setNumber($number);
    $result = $prime->run();

    if($result){
        echo "Is a prime number\n";
    }else {
        echo "Is not a prime number\n";
    }
}catch (\Exception $e){
    echo "Result: ". $e->getMessage() . "\n";
}
