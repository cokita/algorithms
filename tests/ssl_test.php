<?php
$ch = curl_init("https://siapi.edebe.com.br");

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

$res 			= curl_exec($ch);
$curl_errno 	= curl_errno($ch);
$curl_err 		= curl_error($ch);
$httpcode 		= curl_getinfo($ch, CURLINFO_HTTP_CODE);

if ($curl_errno)
{

    echo "\n\n HTTP_CODE: " . $httpcode;
    echo "<br>";
    echo "ERRO: " . $curl_errno . ": " . $curl_err;
    echo "\n\n\n";

}else{

    echo "\n\n SUCCESS - HTTP_CODE: " . $httpcode;
    echo "\n\n\n";

}

curl_close($ch);