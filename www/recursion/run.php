<?php
require __DIR__.'/../vendor/autoload.php';

$path = isset($argv[1]) ? $argv[1] : '/var/www';

try{
    $recursion = new Recursion\Recursion();
    $recursion->setDir($path);
    $biggest = $recursion->run();
    //print_r($biggest);
    echo "The biggest file is: ".$biggest['file']." with the size: ".$biggest['humansize']."\n";
}catch (\Exception $e){
    echo "Problem: ". $e->getMessage();
}

