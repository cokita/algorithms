<?php

use PHPUnit\Framework\TestCase;
use Recursion\Recursion;

class RecursionTest extends TestCase
{
    public function test_if_set_dir()
    {
        $recursion = new Recursion();
        $recursion->setDir('/var/www/html');

        $this->assertEquals('/var/www/html', $recursion->getDir());
    }

    public function test_validate_dir()
    {
        $recursion = new Recursion();
        $recursion->setDir('/var/www/html');
        $validate = $recursion->validateDir();

        $this->assertTrue($validate);
    }

    public function test_run()
    {
        $recursion = new Recursion();
        $recursion->setDir('/var/www/html');
        $recursion->validateDir();
        $result = $recursion->run();

        $this->assertEquals(['file', 'size', 'humansize'], array_keys($result));
    }
}
