<?php

namespace PrimeNumber;

class Prime {
    private $number;

    public function __construct(){}

    public function setNumber(Int $number)
    {
        $this->number = $number;
    }

    public function getNumber() 
    {
        return $this->number;
    }

    public function run()
    {
        $return = true;
        if(!$this->number)
            throw new \Exception("Please enter a valid number.");

        $count = 0;
        for($x = 1; $x <= $this->number; $x++){
            if($this->number % $x === 0){
                $count++;
            }

            if($count > 2){
                $return = false;
                break;
            }
        }

        return $return;
    }
}
