<?php

namespace Recursion;

class Recursion 
{
    private $dir;
    public function setDir($dir)
    {
        $this->dir = $dir;
    }

    public function getDir()
    {
        return $this->dir;
    }

    public function validateDir()
    {
        if(empty($this->dir))
            throw new \Exception("Please, enter with the directory");

        if(!is_dir($this->dir))
            throw new \Exception("Please, enter a valid directory");
        
        return true;
    }

    public function run()
    {
        $result = false;
        if($this->validateDir()) {
            $files = $this->getFilesFromDir($this->dir);
            if($files){
                krsort($files);
                $result = array_shift($files);
            }
        }
        return $result;
    }

    public function getFilesFromDir($dir) {
        $result = [];
        $biggest = null;
        foreach(scandir($dir) as $filename) {
            if ($filename[0] === '.') continue;
            $filePath = $dir . '/' . $filename;
            if (is_dir($filePath)) {
                foreach ($this->getFilesFromDir($filePath) as $childFilename) {
                    $size = $childFilename['size'];
                    $result[$childFilename['size']] = ['file' => $childFilename['file'], 'size' => $size, 'humansize' => $this->human_filesize($size)];
                }
            } else {
                $size  = filesize($dir."/".$filename);
                $result[$size] = ['file' => $dir."/".$filename, 'size' => $size, 'humansize' => $this->human_filesize($size)];
            }
        }
        return $result;
    }

    /**
     * Take it by internet (php.net)
     * @param $bytes
     * @param int $decimals
     * @return string
     */
    private function human_filesize($bytes, $decimals = 2) {
        $factor = floor((strlen($bytes) - 1) / 3);
        if ($factor > 0) $sz = 'KMGT';
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor - 1] . 'B';
    }

}
