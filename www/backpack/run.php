<?php
$file = "files/file2.csv";
$limit = $argv[1] ? $argv[1] : 219;

$content = str_getcsv(file_get_contents($file), "\n");

$peso = array();
$valor = array();
foreach($content as $line){

   $arr = explode(";", $line);
   if(is_numeric($arr[0])){
     $peso[$arr[0]] = $arr[1];
     $valor[$arr[0]] = $arr[2];	
   }
}

asort($peso);
arsort($valor);

echo "PESOS: \n";
print_r($peso);
echo "VALORES: \n";
print_r($valor);

$arrBag = array();
$totAdd = 0;
$totPesoAdd = 0;
$return = "";

foreach($valor as $kv => $v){
   if($peso[$kv]+$totPesoAdd <= $limit){
      //$arrToAdd = array($kv => array($peso[$kv], $v));
      //array_push($arrBag, $arrToAdd);
      $return .= "ID: ".$kv.", VALOR: ".$v . ", PESO: ". $peso[$kv]." \n";
      $totAdd+=$v;
      $totPesoAdd+=$peso[$kv];
   }
}

print_r($return);
echo "TOTAL VALOR ADICIONADO: ". $totAdd ."\n";
echo "TOTAL PESO ADICIONADO: ". $totPesoAdd ."\n";
