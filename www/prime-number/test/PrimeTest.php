<?php

use PHPUnit\Framework\TestCase;
use PrimeNumber\Prime;

class PrimeTest extends TestCase 
{
    public function test_if_set_number()
    {
        $prime = new Prime();
        $prime->setNumber(10);

        $this->assertEquals(10, $prime->getNumber());
    }

    public function test_ifnot_set_number()
    {
        $this->expectException(\Exception::class);
        $prime = new Prime();
        $result = $prime->run();
    }

    public function test_if_is_prime()
    {
        $prime = new Prime();
        $prime->setNumber(7);
        $result = $prime->run();
        $this->assertTrue($result);
    }

    public function test_if_isnot_prime()
    {
        $prime = new Prime();
        $prime->setNumber(22);
        $result = $prime->run();
        $this->assertFalse($result);
    }
}
