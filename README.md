# DOCKER - PHP 7.3 + Apache + SqlSrv

## Instalation

Clone this repository, enter the directory, copy the file .env.exemple for .env.

In file config/vhosts/default.conf -> update the configuration from vhost that execute in machine.

Still in the directory, execute `docker-compose up -d`.

```shell
git clone https://cokita@bitbucket.org/cokita/algorithms.git
cd algorithms/
cp .env.exemple .env
docker-compose up -d
```

If you want change the port, change in file docker-compose.yml, 
download the NGINX, in the directory `/etc/nginx/conf.d/`, create a file `namesystem.conf` with content:

```
 server {
         client_max_body_size 40M;
         listen 80;
         server_name namesystem.whatever.com;
 
         location / {
                 proxy_set_header X-Forwarded-For $remote_addr;
                 proxy_set_header Host $http_host;
                 proxy_pass http://namesystem.whatever.com:8083;
         }
 }
```

Change the server_name for the name that you put in your vhosts in docker. Put the name in the proxy_pass too.
The port is the same that you put in docker-compose.yml.

Restart the Nginx: `systemctl restart nginx.service`
